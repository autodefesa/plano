# TODO

* [ ] Apresentação:
    * [ ] Chave OpenPGP.
    * [ ] Desenvolver um parágrafo em "Motivação" sobre "modelo de fracasso".
    * [ ] Adicionar o conteúdo nas seções Exemplos, Exemplos Históricos.
    * [ ] Tradução para o inglês e espanhol.
    * [ ] Migrar de reStructuredText para Markdown.
* [ ] Protocolo:
    * [ ] Threat index: distância em quantidade de ataques necessários para ser
          0wnado/a (0-days, etc).
    * [ ] Metadados.
    * [ ] Serialização.
    * [ ] Suporte a certificação mediante assinaturas digitais das entidades.
    * [ ] Demo.
* [ ] Planilha:
    * [ ] Incluir estatísticas para ajudar na calibração de probabilidades
          (Atlas da Violência, Mapa da Violência, etc).
    * [ ] Incluir referências para documentação explicativa.
    * [ ] Explicar que a análise de risco é uma ferramenta para ajudar, não
          para levar à paralisia dada a quantidade enorme de ameaças.
* [ ] Implementação:
    * [ ] Agente buscador: harvesting, parsing.
    * [ ] Wizard / gerador de modelos de ameaças.
    * [ ] RESTful inclusa.
    * [ ] Documentação FLOSS online (explicação da metodologia e templates de políticas de SI).
    * [ ] Threat track: categorização única das ameaças numa biblioteca online
          montadora de modelos de ameaça / documentações.
* [ ] Especificações:
    * [ ] Criptosindicatos:
        * [ ] Specs variadas, usando Viable Systems Model, Círculos
              Concêntricos, Protocolo de Ação Coletiva, Criptosindicatos
              Minimalistas etc.
        * [ ] Levar em conta o desacoplamento, "separation of concerns",
              autonomia e distribuição de recursos em criptosindicatos onde há
              igualdade, horizontalidade e transparência interna etc.
        * [ ] Incluindo:
            * [ ] Bootstrap inicial:
                * [ ] Canais de comunicação com segurança necessária.
                * [ ] Espaço para armazenamento com segurança mínima.
                * [ ] Processo básico / Termo de Cooperação / Acordo Comum / Protocolo:
                    * [ ] Segurança da Informação.
                    * [ ] Tomada de decisões.
                    * [ ] Entrada e saída (voluntária, involuntária, AWOL) e perda de acesso.
                    * [ ] Guia de Trabalho (HOWTO).
                    * [ ] Plano de Postura: como se portar, como se colocar.
            * [ ] Modelo de Segurança pessoal e coletivo.
            * [ ] HOWTO: compartilhamento de segredos via repositório:
                * [ ] Keyringer com pseudônimos OpenPGP/SSH e/ou KeepassXC.
                * [ ] Git+tor+ssh+gitlab e/ou syncthing+tor.
            * [ ] Garantias Canárias.
            * [ ] Procedimentos Emergenciais:
                * [ ] Dead People's Switch.
                * [ ] Suportes de terceiros pré-combinados (contábil, jurídico, médico).
                * [ ] Definição de escopo: o que fazer e até onde ir?
