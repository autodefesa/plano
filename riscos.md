# Análise de Riscos

## Planilha de Autodefesa

A Planilha de Autodefesa é um modelo pronto para ser adaptado por
pessoas e organizações no momento de fazer um Plano de Segurança e
Análise de Risco.

[![image](/_static/images/planilha.png)](/_static/planilha-autodefesa-latest.ods)

Baixe [aqui](_static/planilha-autodefesa-latest.ods) a Planilha de
Autodefesa no formato aberto do [LibreOffice](https://libreoffice.org).

### Cuidados com a Planilha

1. Evite incluir nomes próprios na planilha, de pessoas e organizações.
   A planilha pode vazar por alguma falha de segurança; quanto mais
   genérica ela estiver, melhor.
2. Antes de compartilhar a planilha, certifique-se de remover seus
   metadados usando ferramentas como o [MAT](https://mat.boum.org).

## Referências adicionais

Trabalhos relevantes sobre análise de riscos:

* [Relógio do Juízo Final (Doomsday
  Clock)](https://thebulletin.org/doomsday-clock/)
  ([sobre](https://pt.wikipedia.org/wiki/Rel%C3%B3gio_do_Ju%C3%ADzo_Final)).

Outros padrões existentes sobre gestão de riscos:

* [Modo a prueba de riesgos](https://modeladoriesgos.asuntosdelsur.org/):
  herramienta de modelado de riesgos de seguridad digital para activistas y
  organizaciones de la sociedad civil.

* [Risk management - Wikipedia](https://en.wikipedia.org/wiki/Risk_management).

* Padrões ISO:
    * [ISO - ISO 31000:2018 - Risk management ---
      Guidelines](https://www.iso.org/standard/65694.html).
    * [ISO - ISO Guide 73:2009 - Risk management ---
      Vocabulary](https://www.iso.org/standard/44651.html).
    * [ISO - ISO/TC 292 - Security and
      resilience](https://www.iso.org/committee/5259148.html).
    * [ISO - ISO/TC 262 - Risk
      management](https://www.iso.org/committee/629121.html).
    * [ISO 31000:2018(en), Risk management ---
      Guidelines](https://www.iso.org/obp/ui#iso:std:iso:31000:ed-2:v1:en).

* FFIEC:
    * [The FFIEC Cybersecurity Assessment Tool: A Framework for Measuring
      Cybersecurity Risk and Preparedness in the Financial
      Industry](https://digitalguardian.com/blog/ffiec-cybersecurity-assessment-tool-framework-measuring-cybersecurity-risk-and-preparedness).
    * [FFIEC Cybersecurity
      Awareness](https://www.ffiec.gov/cyberassessmenttool.htm).
    * [FFIEC Cybersecurity Assessment Tool - May
      2017](https://www.ffiec.gov/pdf/cybersecurity/FFIEC_CAT_May_2017.pdf).
