# Introdução

## Objetivos

Esta modelagem de ameaças tentar cobrir as seguintes lacunas:

0. Produzir um Modelo de Ameças / Plano de Autodefesa simples e efetivo
   que sirva tanto para indivíduos, organizações, softwares ou
   processos.
1. Que possa ser usado tanto por não-especialistas em segurança assim
   como desenvolvedores/as de software, analistas de segurança e ainda
   ser interpretada por máquina (similar à licenças Creative Commons).
2. Que permita facilmente a publicação de documentos de ameaças,
   mitigações e ativos assim como a construção de bancos de dados ou a
   indexação desses elementos em motores de busca.
3. Facilitar a auditoria e a divulgação dos reais benefícios de
   aplicações e procedimentos de segurança para o público geral.
4. Padronizar a descrição de medidas de segurança e vulnerabilidades
   presentes nas aplicações.
5. Produzir modelos de ameaças práticos e acessíveis para qualquer
   pessoa melhorar sua segurança.
6. Colocar a segurança da informação no mundo da web semântica.

## Motivação

Esse não é mais um guia com uma coleção de *apps de segurança*. Apesar
da frequência quase diária que aplicativos de segurança da informação
são lançados, pouco se fala de como e quais ameaças eles procuram
mitigar. Muitas dessas ferramentas se aproveitam do *hype* da segurança
informação e não apresentam medidas efetivas ou possuem vulnerabilidades
estruturais que invalidam o seu uso enquanto sistema de proteção.

Dentro de um aspecto maior, a resposta a pergunta \"Qual é seu modelo de
ameaça?\" soa sempre vaga e endereçando apenas uma ou outra
vulnerabilidade. Enquanto que ela deveria ser algo tão trivial como a
resposta sobre a sua missão ou objetivos. Uma organização que não leva
em consideração quais são as ameaças e os ataques possíveis contra os
seus objetivos já está vulnerável. Das ruas aos negócios, o único ensino
(anti)motivacional que se abstraí é: vacilou é graxa.

Em geral, quando se discute segurança, os especialistas do mercado
apontam ferramentas e soluções a serem usadas. Mas, mais importante do
que adotar um conjunto de ferramentas, é ter ciência de quais são os
riscos que uma pessoa ou organização corre e quais medidas (aplicativos,
operações) devem ser tomadas. Por essa razão, o modelo de ameaças é um
elemento fundamental e indispensável para ter uma segurança efetiva.
