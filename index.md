# Plano de Autodefesa

* **Modelagem e Especificação em Segurança Operacional - OPSEC**
* Acesso [Onion
  Service](http://guavtnmvsbowzbkcuztte5pzeiqi5vqmmhi33npnk7t22jjupdxlysid.onion)
  ([sobre](https://community.torproject.org/onion-services/)).

Esta é uma iniciativa com foco para quem já tem algum contato com o tema
da Segurança Operacional ou Segurança da Informação. Se você procura o
básico, consulte o [Guia de Autodefesa
Digital](https://guia.autodefesa.org).

# Índice

```{toctree}
:maxdepth: 1
introducao.md
riscos.md
specs.md
referencias.md
teoria.md
meta.md
todo.md
```
