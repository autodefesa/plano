# Referências

## Modelagem de ameaças

O simples fato de ser listada como *referência* não significa que seja
endossada.

* [DREAD](https://en.wikipedia.org/wiki/DREAD_(risk_assessment_model)): criado
  pela Microsoft e descontinuado em 2008, é também um sistema de modelagem de
  ameaça e um mnemônico. Ele é dividido em cinco categorias: Damage,
  Reproducibility, Exploitability, Affected users e Discoverability (DREAD).

* [STRIDE](http://msdn.microsoft.com/en-us/magazine/cc163519.aspx): sucessor do
  DREAD, framework criado pela Microsoft voltado para a elaboração de modelos
  de ameaças para softwares. O sistema é dividido em seis categorias de
  ameaças: Spoofing, Tampering, Repudiation, Information Disclosure, Denial of
  service e Elevation of privilege.

* [TRIKE](http://octotrike.org/)
  ([resumo](https://wiki.openitp.org/cts:4:ella_threat_modelling)): criada em
  2006, é uma metodologia de modelagem open source. Utiliza apenas duas
  categorias de ameaça: Elevação de Privilégio ou Negação de Serviço. A versão
  mais recente (2.0), baseada em palestras, não está disponível.

* [MISP - Malware Information Sharing Platform & Threat Sharing](https://github.com/MISP/MISP).

* [Security risk levels defined | Drupal.org](https://www.drupal.org/security-team/risk-levels).

* [OCRG1.1:Application Threat Modeling - OWASP](https://www.owasp.org/index.php/OCRG1.1:Application_Threat_Modeling).

* [Wikpedia - SWOT Analysis](https://en.wikipedia.org/wiki/SWOT_analysis).

* [OpenIntegrity](https://openintegrity.org).

* [EFF SSD - Introduction to Threat Modeling](https://ssd.eff.org/en/module/introduction-threat-modeling).

* [Threat Assessment & Remediation Analysis (TARA)](https://www.mitre.org/sites/default/files/pdf/11_4982.pdf).

* [Seasponge](https://mozilla.github.io/seasponge) ([código](https://github.com/mozilla/seasponge), [sobre](https://threatpost.com/students-build-open-source-web-based-threat-modeling-tool/111959)).

* [SAFETAG - Security Auditing Framework and Evaluation Template for Advocacy Groups](https://safetag.org).

* [ISO/IEC 27000 (ISO27K)](https://en.wikipedia.org/wiki/ISO/IEC_27000-series): padrão ISO/IEC para segurança da informação.

* [X.1051 : Information technology - Security techniques - Code of practice for Information security controls based on ISO/IEC 27002 for telecommunications organizations](https://www.itu.int/rec/T-REC-X.1051):
    * [ITU-T Rec. Series X Supplement 13 (09/2018) ITU-T X.1051 - Supplement on information security management users' guide](https://www.itu.int/rec/dologin_pub.asp?lang=e&id=T-REC-X.Sup13-201809-I!!PDF-E&type=items)

## Exemplos de modelos de ameaça

Alguns exemplos de documentos de modelo de ameaça para software:

* [The Design and Implementation of The Tor Browser](https://www.torproject.org/projects/torbrowser/design/).
* [Tor design](https://svn.torproject.org/svn/projects/design-paper/tor-design.html#subsec:threat-model).
* [Mailpile](https://github.com/mailpile/Mailpile/wiki/Threat-model).
* [Protonmail](https://blog.protonmail.ch/protonmail-threat-model/).
* [Tails](https://tails.boum.org/contribute/design/#index2h2).
* [I2P](https://geti2p.net/en/docs/how/threat-model).
* [Freenet](https://wiki.freenetproject.org/Threat_model).
* [Cryptocat](https://github.com/cryptocat/cryptocat/wiki/Threat-Model).
* [Pond](https://pond.imperialviolet.org/threat.html).
* [TextSecure](https://github.com/WhisperSystems/TextSecure/issues/782).

## Políticas de segurança

Algumas referências e modelos de política de segurança:

* [Organizational Security Policy | Access Now Digital Security Helpline Public Documentation](https://communitydocs.accessnow.org/370-Organizational_Security_Policy.html).
* [Frontline Policies](https://frontlinepolicies.openbriefing.org/home).
* [Method: Organizational Policy Review | Safetag](https://safetag.org/methods/organizational_policies).
* [Information Security Policy Templates | SANS Institute](https://www.sans.org/information-security-policy/).
* [SDA - Safe and Documented for Activism](https://sdamanual.org/).  * [lfit/itpol: Useful IT policies](https://github.com/lfit/itpol/).
* [tailscale/policies: Security policies for Tailscale](https://github.com/tailscale/policies).
* [Cybersecurity Assessment Tool / Ford Foundation](https://www.fordfoundation.org/work/our-grants/building-institutions-and-networks/cybersecurity-assessment-tool/).
* [SecurityManagement - Debian Wiki](https://wiki.debian.org/SecurityManagement).
* [OpenSSL Security Policy](https://www.openssl.org/policies/general/security-policy.html).
* [Providers' Commitment for Privacy](https://policy.fluxo.info/policy/).
* [Risk analysis for Security Trainings - Tor Project](https://community.torproject.org/training/risks/).
* [Traffic Light Protocol (TLP)](https://www.first.org/tlp/) ([versão CISA](https://www.cisa.gov/news-events/news/traffic-light-protocol-tlp-definitions-and-usage),
  [artigo na Wikipedia](https://en.wikipedia.org/wiki/Traffic_Light_Protocol)).
* [Chatham House Rule](https://en.wikipedia.org/wiki/Chatham_House_Rule).
* [OWASP Cheat Sheet Series](https://cheatsheetseries.owasp.org).
