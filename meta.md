# Meta

## Licençiamento

Não havendo menção contrária, todo o conteúdo é disponível de acordo com

* [Creative Commons --- Attribution-ShareAlike 3.0 Unported --- CC
  BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
* [Definition of Free Cultural
  Works](http://freedomdefined.org/Definition).
* Logo: [Lock dissolving into
  butterflies](https://openclipart.org/detail/270024/lock-dissolving-into-butterflies).

## Baixando o repositório

URL pública, somente leitura:

    git clone --recursive https://0xacab.org/autodefesa/plano

## Espelhando o repositório

Para fazer o espelhamento, configure o
[python-sphinx](http://sphinx-doc.org) usando o repositório baixado:

    cd plano && make web

## Contribuindo com o repositório

Caso você seja mantenedor/a deste repositório e queira manter suas
contribuições anônimas, certifique-se que as seguintes linhas estejam
presentes no `.git/config` do mesmo:

    [user]
      name       = Autodefesa
      email      = autodefesa@autodefesa.org
      signinkey  = 0xFINGERPRINT

Se você preferir, também pode identificar suas mudanças através de nome
e email próprios, porém é recomendado utilizar uma chave OpenPGP para
assinar tags do repositório.

Assinaturas a cada commit também são possíveis:

    git config commit.gpgsign true

Referências:

* [Git - Signing Your Work](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work).
* [public key encryption - Is there a way to "autosign" commits in Git with a GPG key? - Stack Overflow](http://stackoverflow.com/questions/10161198/is-there-a-way-to-autosign-commits-in-git-with-a-gpg-key)

## Criando uma tag assinada

Exemplo de comando:

    GPG_AGENT_INFO="" git tag -s 0.1 -m "Versao inicial"

## Verificando uma tag assinada

    git tag -v 0.1

## Verificando um commit assinado

    git log --show-signature -n 1
    git verify-commit HEAD
