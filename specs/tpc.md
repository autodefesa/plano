# Console Físico Confiável

## Sobre

A parte introdutória deste texto é uma tradução de [Trusted Physical
Console](https://web.archive.org/web/20180914153944/http://cmrg.fifthhorseman.net/wiki/TrustedPhysicalConsole)
([versão
10](https://web.archive.org/web/20160927004824/http://cmrg.fifthhorseman.net/wiki/TrustedPhysicalConsole?version=10)).

## Introdução

As atuais boas práticas do CMRG sugerem usar um único computador, bem
afinado como sua interface de usuário imediata (um \"Console Físico
Confiável - CFC\" ou \"Trusted Physical Console\", em inglês). A partir
desse computador, você pode se conectar em qualquer outra máquina numa
rede para fazer o que precisa ser feito.

## Motivação

### Segurança

Se você usa computador todo dia, talvez você queira ter certeza que a
máquina que você está usando é segura. Dado o fato como são complicadas
essas máquinas, é razoável perguntar:

* Eu sei quais softwares estão instalados nessa máquina?
* Eu tenho algum nível de controle sobre essa máquina?
* Se essa máquina sabe meus dados ou informações pessoais (incluindo senhas e
  outros tokens de autenticação), eu posso confiar que essa informação não será
  vazada?

Usando um CFC lhe dará a oportunidade de responder essas questões
afirmativamente e com confiança.

### Personalização

Talvez você esteja frustrado por estar sempre mudando de interface de
usuário e de experiência de usuário:

* É o Ctrl+C ou Comando+C para copiar o texto?
* Devo clicar no canto superior esquerdo ou canto superior direito para fechar
  uma janela?
* Aonde estão os favoritos do meu navegador web?
* A minha ferramenta favorita já está instalada aqui e é funcional? É a versão
  que eu quero? Posso confiar nela?
* Se eu mexo nas configurações para deixar do meu jeito, eu poderei usá-las de
  novo na próxima vez que usar essa máquina?

Usando um CFC, você pode ser mais eficiente no seu trabalho e gastar
menos tempo descobrindo o que está acontecendo. O tempo perdido
personalizando seu CFC para se encaixar nos seus hábitos de trabalho e
requerimentos serão recompensadores, pois você sabe que será capaz de
usar essas personalizações quando usar essa máquina no futuro.

## Boas Práticas

### Software Livre

Usar o máximo possível de [Software
Livre](http://www.gnu.org/philosophy/free-sw.html) no seu CFC. O
software livre fornece-lhe código auditável, a possibilidade de
customização para seu uso particular e reduz o impacto de estar
dependente de um vendedor, o qual você não compartilha seus valores,
política ou incentivos financeiros.

O Software Livre também têm a vantagem que é geralmente sem custo
monetário. Isso é uma vantagem técnica, porque permite você analisar as
ferramentas antes de usá-las sem risco de perda financeira. E também
oferece uma vantagem econômica se você se comprometer a aprender a
ferramenta, pois não será necessário pagar para aprender a usar durante
a aquisição ou posteriormente na atualização. Essas vantagens econômicas
são pequenas em comparação com as vantagens técnicas e políticas do
software livre, porque a maior parte do custo de uma ferramenta é seu
tempo gasto aprendendo como manipulá-la para fazer o que você quer
rapidamente, de forma confiável e conveniente.

Quando você se encontra usando um software não livre, você deve se
perguntar:

* Eu preciso dessa ferramenta?
* Existem ferremantas livres que podem fazer essa mesma tarefa?
* Se sim, quais são os fatores que me impedem de usar as ferramentas
    livres? Eu já comuniquei ao time de desenvolvedores sobre esses
    fatores dessas ferramentas (ou para aqueles que teriam a capacidade
    para tal)?
* Se não há software livre disponível, qual é a ferramenta livre
    disponível mais parecida com ela? O que mais preciso para satisfazer
    minhas necessidades? Eu já comuniquei essas necessidades para algum
    time de desenvolvimento dessas ferramentas?
* Se nada livre chega nem perto, eu documentei e publiquei o fato que
    eu estou usando ferramenta não livre e desejaria usar uma ferramenta
    livre?

### Customização

Customize seu CFC! Leva um certo de investimento de tempo de sua parte,
mas esse pouco tempo pode recompensar se você usa um computador como seu
CFC. Pelo motivo do CFC é sua interface primária no mundo digital, você
sempre pode contar com a disponibilidade de suas customizações e
atalhos, então, o tempo gasto customizando é o tempo que você salvará ao
longo dos anos evitando fazer essas mesmas customizações.

Se você se encontra fazendo uma tarefa no CFC que parece ser chata e
repetitiva, você deve se perguntar:

* Quando foi a última vez que eu fiz alguma coisa parecida com isso?
* Como são parecidas essas tarefas? Como elas são diferentes?
* If i could have the computer figure out how to take care of tasks like this,
  what would i ask it to do? Can i make this framing question fairly concise
  while keeping it clear?
* Com a pergunta acima de enquadramento em mente, eu já procurei por softwares
  que atendem essa necessidade?
* Eu já pedi ajuda para outras pessoas?
* Se eu não consigo encontrar esse software, eu já publiquei essa pergunta de
  enquadramento num fórum frequentado por pessoas que pode ter tarefas
  similares?
* Eu já publiquei essa pergunta de enquadramento num fórum frequentado por
  pessoas que talvez faça esse tipo de ferramentas?
* Eu mesmo já tentei fazer uma ferramenta similar?

### Envolvimento da Comunidade

Compartilhar suas customizações e seus hábitos de trabalho \--
juntamente com a lógica por trás deles \-- de forma bem sucedida (ou mal
sucedida) com outros usuários é um bom caminho para ajudar e apoiar a
comunidade. Também ajuda outras pessoas saber quais dicas você pode
estar interessado, que por sua vez faz sua experiência no computador ser
mais eficiente.

Conforme você estabelece o uso do seu CFC regularmente, procure por
amigos, aliados e vizinhos que fazem o mesmo trabalho ou que utilizem
ferramentas semelhantes. Peça conselhos e ofereça também quando os
outros perguntarem para você. Não há duas pessoas que usam seus
computadores da mesma forma, mas você pode construir uma rede de
colaboradores confiáveis que se enriquecem mutuamente as suas
experiências com as máquinas.

Ter um círculo de amigos que trabalham com as mesmas ferramentas é
divertido!

### Comunicações Criptográficas

Se comunicar numa rede altamente intermediada abre suas comunicações
para o sniffing (pessoas xeretando as suas comunicações quando não
deveriam) e spoofing (pessoas se passando por você ou tentando se passar
por uma entidade que você acha que está se comunicando). Os protocolos
criptográficos ajudam você evitar isso ao oferecer uma matemática
poderosa projetada para a privacidade (contra o sniffing) e autenticação
(contra o spoofing). Mas a lógica por trás delas só funciona quando você
está totalmente sob controle do canal final da comunicação.

Usar um CFC lhe dará a oportunidade de ter o controle da ponta da sua
comunicação, mas apenas se prestar atenção para o que está fazendo. Por
exemplo:

* Ao usar OpenSSH para conectar num computador remoto, você deve sempre ter
  certeza que está conectando a partir do seu CFC -- não se conecta por ssh
  pulando de uma máquina para outra, por exemplo, (veja também [Boas práticas
  ao usar ssh](http://lackof.org/taggart/hacking/ssh/))
* Quando conectado num website rodando HTTPS, conecte do seu navegador rodado
  no seu CFC e evite usar proxies (a não ser que apenas opere no nível de IP)
  ou serviços de redirecionamento.
* Tenha certeza de verificar a identidade da parte remota de forma segura! Com
  SSH, isso significa verificar a chave do host. A primeira vez que se conectar
  será mostrado uma impressão digital, na qual o administrador do host deverá
  ter fornecido para você de anteriormente. Com o HTTPS ou outro serviço
  baseado em HTTPS, isso significa ter a certeza que o certificado remoto é
  válido e que você confia que o emissor apenas emite certificados legítimos.
* Esteja atento aos tipos de cifras usados para criptografar as comunicações
  que você espera que seja privadas. Por exemplo, o TLS
  RFC:2246#section-6.2.3.1 permite uma cifra NULA, que não mantém sua
  comunicação privada. Se você não liga para privacidade (p.e.  quando você
  está acessando um sistema de controle de revisão publicamente acessível \--
  você quer que seja autenticado, mas não liga se as pessoas podem ver o que
  você está enviando ou recebendo), usando a cifra NULA está ok. But when you
  do care about privacy, do you know that your communications are using
  something more cryptographically sound?

### Backups

Faça o backup do seu CFC. Isso não pode ser subestimado. Porque a
natureza do seu CFC indica que você está armazenando informação
confidencial nele (chaves privadas, senhas e outras formas de
identificação digital, assim como registros financeiros e políticos), é
altamente recomendável que você mantenha os seus backups criptografados.
Há muitas maneiras de se fazer isso. Algumas maneiras populares:

* [backupninja](http://dev.riseup.net/backupninja/): backup
    criptografado incremental através da rede
* [cryptsetup](http://www.saout.de/tikiwiki/tiki-index.php?page=cryptsetup)
    com [rsync](http://samba.anu.edu.au/rsync/): faça backup dos seus
    dados para um disco externo criptografado

### Monitoramento do Sistema

Esteja atento aos sinais vitais do seu CFC. Se algo indica que está em
vias de pifar, tenha um plano de restauração pronto. No decorrer do uso
do CFC, faça uma nota mental (ou digital) do que está acontecendo, mesmo
se você não entender especificamente o que os sinais significam. A mente
humana é excelente para reconhecer padrões e se você prestar suficiente
atenção nas pistas, poderá ver um padrão. Pelo menos, você estará
preparado para perceber uma mudança no padrão, o qual pode indicar que
alguma coisa está errada ou funcionando de forma estranha.

### Peças de Reposição

Se uma parte do seu CFC falhar, você está em apuros. O ambiente
customizado, as habilidade que você construiu e as várias outras formas
de identificação pessoal podem ficar indisponíveis para você até que o
seu dispositivo seja reparado e esteja funcional de novo. Você pode
achar que vale a pena ter por perto uma máquina similar para servir de
peças de reposição. Se isso não é uma possibilidade, tente saber de
antemão com quem você pode entrar em contato que pode ter as peças,
assim você não estará completamente perdido quando o desastre acontecer.

## Desvantagens

Há problemas com o modelo CFC de computação, é claro.

### Despesas

Nem todo mundo pode dispôr de uma máquina portátil. No entanto, os
laptops estão se tornando muito mais baratos e os telefones celulares
estão se tornando mais poderosos. Em algum momento num futuro não muito
distante, os caminhos de desenvolvimento desses dois candidatos para CFC
podem se cruzar, colocando a computação estilo CFC dentro do alcançe da
maioria das pessoas.

### Inconveniência

No momento, computadores decentes portáteis são ainda muito grandes e
desajeitados para carregar com certa facilidade. Curiosamente, o poder
de processamento, memória RAM e capacidade de disco (as especificações
tradicionais pelas quais os computadores são medidos) não são mais os
fatores limitadores.

### Interface de Usuário

Para fazer um trabalho pesado com computador numa máquina portátil, nada
bate um teclado de tamanho razoável e decente e pelo menos um monitor de
vídeo XGA (1024x768). Mas isso são equipamentos grandes e difíceis de
serem carregados juntos com você. Você pode pensar em interfaces
melhores ou outras maneiras de torná-los mais convenientes para
transportar?

### Durabilidade

Quanto mais durável uma máquina é, mais pesada ela é. Essa é um troca
ruim para pessoas que querem levar seu CFC com elas para onde quiserem.
No entanto, com o aparecimento de novas tecnologias, alguns sistemas
podem ficar mais leves \'\'e\'\' mais duráveis. Por exemplo,
armazenamento em estado sólido é tanto mais leve e mais durável que um
disco rígido tradicional.

### Consumo de Energia e Baterias

As baterias continuam uma porcaria. Processadores portáteis e hardwares
modernos consomem menos energia agora do que antes (com a exceção de
adaptadores wireless que precisam de energia para transmissão a rádio),
mas eles podem ainda consumir a bateria rapidamente em questão de
algumas horas. Baterias maiores adicionam mais peso na sua máquina.

### Ponto Único de Falha

Se você usa um CFC, não faz backups regularmente e ele
quebrar/morrer/cair no banheiro, você pode ter um longo caminho para
restaurar. Faça backup do seu CFC!

### Estranhamento Social

Se você usa uma máquina para boa parte da sua interação com o mundo
exterior, usá-la regularmente e levar para a maior parte dos lugares,
não será estranho se criar um vínculo com ela. Sim, é ridículo. Mas não
é surpreendente, dado que somos seres sociais e que nós tendemos a
tratar nossas ferramentas mais familiares (roupas, bicicletas, carros,
etc) como extensões de nós mesmos. Esteja preparado para algum nível de
descrença ou ridicularização social de pessoas que não têm uma forte
simbiose com uma máquina comparativamente complicada.

## Checklist

O que levar em conta:

* [Linux workstation security
  checklist](https://github.com/lfit/itpol/blob/master/linux-workstation-security.md).
* Criptografia de disco (FDE - Full Disk Encryption).
* Cold e warm boot: desligamento, suspensão ou hibernação; descarga de
  capacitores; DDR3.
* Evil maid e [Bootless](https://bootless.fluxo.info).
* BadBIOS e Firmwares Livres.
* Air gapping e BadUSB.
* Checar se [dhcp clients vazam
  hostname](https://labs.riseup.net/code/issues/7688) / MAC anonymization.
* Inicialização de módulos TPM.
* Inspeção manual do hardware.
* Grsecurity, PaX e virtualização.
* Senhas na BIOS.

### Homologação de nova unidade

Ao obter um novo TPC:

* Memtest e outros [testes de hardware](https://en.wikipedia.org/wiki/Burn-in).
* Remoção de hardware proprietário e/ou invasivo.
* Substituição do firmware.
* Mods: tapar ou desconectar câmera, retirar microfone, etc.
* Avaliação das condições operacionais do hardware, como:
    * Pasta térmica do processador.
    * Funcionamento da ventuinha.
    * Condições da carcaça, fazendo remendos caso necessário.
    * Vida útil da bateria.

## Auditoria periódica

Repassar checklist e procedimento de homologação, incluindo testes como

    paxtest blackhat
    sudo aa-status

## References

* [Trusted computing base (TCB)](https://en.wikipedia.org/wiki/Trusted_computing_base).
* [Linux workstation security (itpol/linux-workstation-security.md](https://github.com/lfit/itpol/blob/master/linux-workstation-security.md)
  ([discussão](https://www.reddit.com/r/linux/comments/oj020w/found_a_pretty_comprehensive_workstation_security/)).
* [Securing Debian Manual](https://www.debian.org/doc/manuals/securing-debian-manual/index.en.html).
