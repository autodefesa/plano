# Avaliação de código

Como avaliar determinada aplicação:

* Se devemos ou não executá-la.
* Em qual ambiente e grau de isolamento ela deve ser executada.
* Como verificar a integridade de código fonte e binário.
* O que fazer caso ela seja proprietária e mesmo assim precisarmos
    executá-la ou utilizá-la?

## Malícia ou erro

Pela [Navalha de
Hanlon](https://en.wikipedia.org/wiki/Hanlon%27s_razor), não temos como
afirmar se um bug num software é intencional ou resultado de um erro de
programação. Na verdade, essa questão não importa contanto que se
descubra o bug.

Assumimos então as necessidades de:

1. Auditoria de padrões, protocolos e implementações (código).
2. Verificação de software obtido quanto à garantia de origem (sem
   alterações durante download ou alterações à revelia do time de
   desenvolvimento).
3. Estabelecer vínculos de confiança com times de desenvolvimentos.

A realidade, no entanto, é desoladora:

1. Auditoria é cara e impraticável em vastas quantidades de código.
2. Times de desenvolvimento são anônimos ou inexistem laços de
   confiança entre os próprios/as desenvolvedores e seus usuários/as;
   mesmo se existirem, é difícil saber se o time de desenvolvimento
   está colaborando com implantadores de backdoors (via gag orders,
   etc).
3. A maior parte dos softwares não é assinada digitalmente.

A segurança num mundo complexo que ainda permite a interação social se
dá pela redução de danos. A seguir são delineados diversos níveis de
execução de software.

### Padronização

A seguir há uma simples padronização de níveis de verificação e execução
de código cujo objetivo é estabelecer um nível de proteção de dados.
Adicione um tempero de conexão, armazenamento ou processamento
criptografado (e nos conte como você fez!) para aumentar ainda mais sua
segurança!

## Verificação de código

Em níveis decrescentes de confiabilidade:

* V1. O código é auditado por quem o executa. Viável para uma pequena porção de
  código sensível, inviável para todo um sistema ou infraestrutura.
* V2. O código é auditado por um grupo confiável. Maior abrangência de código,
  porém ainda inviável para cobrir qualquer aplicação prática.
* V3. A integridade do código é verificada por procedimento criptográfico
  (assinatura digital) em que existe algum caminho de verificação ("web of
  trust" ou similar) e procedimento de confiabilidade (mentoring, etc) dos/as
  mantenedores/desenvolvedores do software.
* V4. A integridade do código é verificada por procedimento criptográfico
  (assinatura digital) independente de haver caminho de verificação ou
  atestação de mantenedores e desenvolvedores/as.
* V5. Checagem básica de integridade de código (obtenção via conexão
  criptografada verificada ou checagem simples de hash para evitar alterações
  de código em trânsito (`QUANTUMINSERT` e afins).
* V6: TOFU (Trust on First Use).
* V7. Sem checagem de integridade.

## Execução de código

Em níveis decrescentes de isolamento:

* R1. O código roda numa máquina física específica (alto isolamento).
* R2. O código roda numa máquina virtual específica (médio
    isolamento).
* R3. O código roda numa máquina virtual compartilhada (baixo
    isolamento).
* R4. O código roda numa máquina física compartilhada (sem
    isolamento).

## Dados

Níveis de proteção por dados fornecidos apenas a aplicações:

* D1. Confiáveis executadas em primeira pessoa (infra-estrutura própria) e
  dependendo dos níveis de verificação e execução de código):
  a.  Que não compartilham dados com outras aplicações.
  b.  Que compartilham dados com aplicações do mesmo nível.
  c.  Que compartilham dados com aplicações de nível abaixo (degradação de
      nível).
* D2. Confiáveis executadas por terceiros confiáveis e dependendo dos níveis de
  verificação e execução de código:
  a.  Que não compartilham dados com outras aplicações.
  b.  Que compartilham dados com aplicações do mesmo nível.
  c.  Que compartilham dados com aplicações de nível abaixo (degradação de
      nível).
* D3. Não confiáveis executadas em primeira pessoa:
  a.  Que não compartilham dados com outras aplicações.
  b.  Que compartilham dados com aplicações do mesmo nível.
  c.  Que compartilham dados com aplicações de nível abaixo (degradação de
      nível).
* D4. Não confiáveis executadas por terceiros:
  a.  Que não compartilham dados com outras aplicações.
  b.  Que compartilham dados com aplicações do mesmo nível.
  c.  Que compartilham dados com aplicações (degradação total de segurança
      e privacidade).

### Exemplo: TPC

O [TPC](tpc) é o instrumento básico de trabalho do/a desenvolvedor e
administrador de sistema (DevOp), sendo importante estabelecer um nível
de confiabilidade para o código executado. Por exemplo:

1. Nível do host: `V3:R3:D1a`, `V3:R3:D1b` e `V3:R3:D1c` são até certo
   ponto aceitáveis.
2. Máquinas virtuais: para cada máquina virtualizada, adotar um nível.

É importante lembrar, contudo, que apesar do bom isolamento de diversas
soluções de virtualização em software livre e aberto, falhas de
segurança podem existir e permitir que uma aplicação \"vaze\" para fora
do ambiente virtual.

### Status

Estado da verificação de autenticidade e integridade em diversas
distribuições.

* [TUF](http://theupdateframework.com).
* BSDs.
* GNU/Linux: kernel e distros.
* Pacotes:
* [Debian\'s Reproducible Builds](https://wiki.debian.org/ReproducibleBuilds).
* [Guardian Project\'s Reproducible
  Builds](https://guardianproject.info/2015/02/11/complete-reproducible-app-distribution-achieved/).
* Por linguagem:
* PHP/composer/packagist.
* Rubygems:
  * [rubygems-openpgp Certificate
    Authority](https://www.rubygems-openpgp-ca.org/).
  * [Security - RubyGems Guides](http://guides.rubygems.org/security/).
  * [A Practical Guide to Using Signed Ruby Gems - Part 1: Bundler ---
    Meldium](http://blog.meldium.com/home/2013/3/3/signed-rubygems-part).
  * [Let's figure out a way to start signing
    RubyGems](http://tonyarcieri.com/lets-figure-out-a-way-to-start-signing-rubygems).
  * [The case against using RubyGems.org in
    production](https://www.honeybadger.io/blog/2013/06/25/stop-using-rubygemsorg-in-production).
* Python.
* Perl.
* Node/npm.

## Git

O git supporta tags e commits assinados. No caso de tags:

    tag="`git describe --abbrev=0 --tags`"
    git tag -v $tag && git checkout $tag

Há uma implementação desta checagem no plugin
[git-check-tags](https://git.fluxo.info/?p=utils-git.git;a=blob;f=git-check-tag;h=HEAD)
(e que também pode ser checada).

### Como avançar a agenda?

Criar um template padrão de mensagem para:

* Advogar a adoção de checagens por padrão nas plataformas de distribuição.
* Advogar releases assinados e obtenção via https para softwares.

### Homologação

Em ambiente isolado:

1. Download.
2. Verificação de integridade.
3. Compilação.
4. Teste.
5. Empacotamento.

### Referências

* [Tor - deterministic builds](https://blog.torproject.org/category/tags/deterministic-builds).
* [VerifyingSignatures -- Qubes OS](https://wiki.qubes-os.org/wiki/VerifyingSignatures).
