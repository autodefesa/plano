# Usando cofres

Nosso modelo de ameaça no nosso caso é um pouco diferente de quem quer
guardar só dinheiro e jóias no cofre.

O mais importante, no nosso caso, seria proteger os dados.

Deixando um laptop desligado/hibernando (isto é, nada de software
suspend) dentro de um cofre mitigaria muitas das ameaças de instalação
de grampos e ataque coldboot:

* Se o cofre for arrombado e a máquina subtraída, paciência, basta restaurar a
  vida a partir de um laptop de backup armazenado num local alternativo.
* O mesmo procedimento pode ser feito caso o cofre tenha sinais de arrombamento
  mas o conteúdo não esteja subtraído.

No momento creio que é apenas nisso que um cofre pode ajudar.

Isso assume que o oponente teria que ser sofisticado o suficiente para
conseguir arrombar o cofre sem deixar sinais. Um ator que tivesse
recursos suficientes, faria assim:

## 1. Método pé-na-porta

* Retiraria o cofre do local, arrombaria usando métodos invasivos (makita,
  martelete, porradaria).
* Uma vez com o cofre aberto, comprometeria a segurança física dos hardwares e
  faria cópia dos discos.
* Colocaria tudo de volta, na mesma posição, num cofre idêntico, restaurando a
  senha a partir de um dump da memória do cofre velho.

## 2. Método elegante

Assumindo que já tenha forçado a cooperação do fabricante ou instalado
um backdoor no cofre por interdição, só precisaria ter acesso físico ao
cofre por um breve período de tempo, sem deixar muitos rastros.

## Eficácia

Ambos os métodos poderiam ser desobertos se houver uma câmera e um
microfone dentro do cofre que emitam um alarme logo que detectarem
alguma atividade estranha. Um RaspberryPI configurado como alarme
presencial poderia fazer esse papel, mas o problema seria ter sinal wifi
ou 4G dentro do cofre.
