# Sexo - Resolução 69

Versão: 2.

## Sobre

Estes são exemplos de recomendações aplicando o ferramental de Segurança
Operacional para relações sexuais, tema sensível e que pode ser abordado
por diversas perspectivas.

Aqui a intenção é esboçar práticas possíveis, baratas e com eficácia
significativas para evitar o contágio de doenças sexualmente
transmissíveis e gravidez indesejada.

Elas levam em conta as práticas em si e que são assumidas
consensualmente entre as partes envolvidas, mas atualmente são
incompletas no que concerne a formas de abordagem do tema com pareceiros
e parceiras, coações, intimidações, abusos, etc.

Elas constam aqui como modelo, exemplos e não como leis ou regras
estritas, servindo também para estabelecer debates sobre essas práticas.

## Objetivo

Especifica prática de sexo não-individual, sobre contato com fluidos e
sobre o uso de preservativos.

* EPI: Equipamento de Proteção Individual.
* EPI Sexual: Equipamento de Proteção para Intercurso Sexual.
* DST ou IST: Doença Sexualmente Transmissível / Infecção Sexualmente
    Transmissível.

## Modelo de ameaças

* Parceiro/a(s) pode(m) estar infectado(s) com DSTs - informação conhecida ou
  desconhecida.
* Múltiplos parceiros/as (simultâneos ou consecutivos).
* Gravidez não planejada.

## Modo Padrão

* **Sexo oral, anal e vaginal apenas com preservativo (fazendo ou
    recebendo)**.
* Sem sexo durante o período menstrual.
* Exames periódicos de DSTs.

## Modo Especial

Dois ou mais parceiros/as *fixos* podem estabelecer um acordo mediante o
qual podem, *apenas* entre eles, praticar sexo sem algumas das
restrições do Modo Padrão, isto é:

* Sexo oral e anal sem preservativo.
* Sexo durante o período menstrual.
* Caso não haja desejo de gravidez: Sexo vaginal *com* preservativo (vide
  resolução EPI) e/ou combinado a métodos contraceptivos alternativos com
  eficácia equivalente (vasectomia, infertilidade, pílula, DIU, etc).

Parceiros podem cobrar uns dos outros/as que realizem e apresentem os
exames periódicos de DSTs.

## Referências

As referências abaixo não correspondem necessariamente a fatos
cientificamente confirmados, porém serviram de base para a criação e
aprimoramento desta especificação.

Uma das dificuldades para criar especificações do tipo é não apenas a
falta de informação consolidade e verificada mas também a quantidade de
informações desencontradas e portanto em disputa, inclusive presente nas
referências utilizadas. Esta limitação, aliás, é um convite para a
redação de um guia mais sólido e completo.

Nada substitui a consulta a profissionais, a sua própria pesquisa e usar
sua consciência para criar seu próprio conjunto de procedimentos, além
de manter um diálogo com suas parceirias sexuais, lembrando que cuidar
de você também é cuidar das outras pessoas.

* [Departamento de Vigilância, Prevenção e Controle das IST, do HIV/Aids das
  Hepatites Virais | Portal sobre aids, infecções sexualmente transmissíveis e
  hepatites virais](http://www.aids.gov.br/)
* [Cómo usar una barrera bucal para tener sexo oral | Eficacia de los condones
  | CDC](https://www.cdc.gov/condomeffectiveness/spanish/Dental-dam-use.html)
* [Dental Dam Use | Condom Effectiveness |
  CDC](https://www.cdc.gov/condomeffectiveness/Dental-dam-use.html)
* [LUAS: Você já ouviu falar de Dental
  Dam?](https://jornalluas.blogspot.ca/2008/02/voc-j-ouviu-falar-de-dental-dam.html)
* ["Sexo seguro entre lésbicas" | Na Ponta dos
    Dedos](https://napontadosdedos.wordpress.com/2011/09/09/sexo-seguro-entre-lesbicas/):
    [1](https://napontadosdedos.wordpress.com/2008/01/17/o-mito-do-sexo-seguro-entre-lesbicas/),
    [2](https://napontadosdedos.wordpress.com/2008/05/15/uma-conversa-com-dra-julieta/),
    [3](https://napontadosdedos.wordpress.com/2008/08/20/mais-dicas-sobre-sexo-seguro-para-lesbicas-e-mulheres-bissexuais/),
    [4](https://napontadosdedos.wordpress.com/2009/04/08/hpv-e-cancer-do-colo-do-utero/)
* [O Bocas: A boca e as doenças sexualmente
    transmissíveis](https://blogobocas.blogspot.ch/2010/11/boca-e-as-doencas-sexualmente.html)
* [As DST's e a Boca | - Odontologia do Trabalho](https://odontotrab.blogspot.se/2011/02/as-dsts-e-boca_06.html).
* [Guia para la práctica de sexo lésbico seguro](http://relatoslesbicos.homestead.com/sexoseguro.html)
* [Sexo oral (super) protegido: cobrir a vulva com plástico filme evita DSTs? - Entretenimento - BOL Notícias](https://noticias.bol.uol.com.br/ultimas-noticias/entretenimento/2017/12/06/sexo-oral-super-protegido-cobrir-a-vulva-com-plastico-filme-evita-dsts.htm)
* [Sexo oral pede proteção redobrada | Minha Vida](http://www.minhavida.com.br/saude/materias/5541-sexo-oral-pede-protecao-redobrada)
* [Sexo oral sem camisinha pode causar câncer de boca](https://www.terra.com.br/vida-e-estilo/saude/saude-bucal/atualidades/sexo-oral-sem-camisinha-pode-causar-cancer-de-boca,0c009b7cbda5b410VgnVCM10000098cceb0aRCRD.html)
