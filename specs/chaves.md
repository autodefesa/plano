# Política de Chaves

## Chaves OpenPGP

O [OpenPGP][] é um [padrão de criptografia][openpgp-std] de uso geral.

Uma introdução é dada [neste guia][manual-cripto], e o presente documento
contém práticas recomendadas para gestão de chaves [OpenPGP][].

Checar também [OpenPGP Best Practices](https://help.riseup.net/en/gpg-best-practices).

[OpenPGP]: https://pt.wikipedia.org/wiki/Pretty_Good_Privacy
[openpgp-std]: https://www.openpgp.org/
[manual-cripto]: https://cripto.fluxo.info/

### Fluxo de Manutenção

* Geração de par de chaves.
* Impressão de fingerprints/cartões de visitas.
* Uso regular da chave.
* Rotação de subchaves.
* Atualização de data de expiração.
* Rotação de par de chaves.
* Revogação de par de chaves antigo.

Bônus:

* Uso de tokens criptográficos de hardware aberto.
* Manutenção de um par de chaves de longo prazo offline usada apenas para
  certificação. Este par pode ser armazenado por exemplo num sistema cifrado
  dentro de um cartão microSD (por exemplo um Tails com volume persistente) ou
  num token. Sua função é assinar as chaves de uso corrente.

### Renovação

Editando a chave:

    gpg --edit-key <fingerprint>

Para a chave principal:

    expire

Para cada chave:

    key <n>
    expire

Exportando a chave para o [keys.openpgp.org][]:

    gpg --export <fingerprint> | curl -T - https://keys.openpgp.org

[keys.openpgp.org]: https://keys.openpgp.org/about/usage

Exportando a chave para um servidor de chaves SKS "clássico" (que estão
deixando de ser utilizados, [em favor do keys.openpgp.org][sks-transition]):

    gpg --send-key <fingerprint>

[sks-transition]: https://keys.openpgp.org/about/news#2019-06-12-launch

Em seguida, escreva para cada lista cifrada:

* [Firma](https://firma.fluxo.info): enviar ao administrador/a.
* [Schleuder][]: `x-add-key:` para o email `nome-da-lista-request@dominio` conforme
  [documentação](https://schleuder.org/schleuder/docs/subscribers.html):

        x-list-name: nome-da-lista@dominio
        x-add-key:

        <chave exportada com armadura ascii>

Em versões anteriores do schleuder, o
[procedimento](http://schleuder2.nadir.org/documentation/v2.2/special_commands.html)
precisa ser feito em duas etapas:

1. Email para <lista-request@example.org> com o comando
   `x-delete-key: <id-da-chave>`.
2. Email para <lista-request@example.org> com o comando `x-add-key` e a
   chave logo abaixo no corpo do email.

Se fizermos apenas `x-add-key` o schleuder acusa que a chave já existe.

O problema dessa abordagem é que ela precisa ser feita por um/a admin da
lista que não seja a pessoa cuja chave está sendo atualizada, senão ela
acaba saindo do loop logo no `x-delete-key`.

Talvez seja um [bug](https://0xacab.org/schleuder/schleuder/issues)?

[Schleuder]: https://schleuder.org/

Finalizando:

* Caso suas chaves sejam disponibilizadas via [WKD][] (Web Key Directory),
  proceda com a atualização correspondente, a depender de como o [WKD][] foi
  configurado para o domínio que você utiliza.

* Se você mantém um registro de mudanças (por exemplo um arquivo
  `~/.gnupg/ChangeLog.md`), este é um bom momento para atualizá-lo.

* Finalmente, faça backups suficientemente seguros das suas chaves.

### Transições

Passos recomendados para chaves de uso difundido (isto é, que estejam
disponíveis amplamente na rede):

* Manutenção das chaves existentes:
    * Caso as chaves atualmente em uso estejam próximas da expiração,
      considere primeiramente renová-la por mais um período (por exemplo
      de um a dois meses), tempo suficiente para que sua nova chave seja
      propagada por aí e as pessoas comecem a utilizá-la.
      Isso pode ser feito conforme o procedimento da seção anterior.
* Criação das novas chaves:
    * Criar a nova chave com `gpg --gen-key`.
    * Caso o [Monkeysphere][] seja utilizado:
        * Criar subchave de autenticação: `monkeysphere gen-subkey -l 4096
          <fingerprint-novo>`.
        * Configurar o período de expiração da subchave de autenticação `gpg --edit-key
          <fingerprint-novo>` e usar o comando `expire` após selecionar a subchave.
        * Assinar a nova chave usando a chave antiga: `gpg --edit-key <fingerprint>
          sign`.
* Publicação das chaves:
    * Subir as chaves para os servidores de chaves.
    * Criar e publicar um atestado de transição: `gpg -u <fingerprint-antigo> -u
      <fingerprint-novo> --armor --clearsign transition.txt`.
    * Notificar círculos próximos de contato.
    * Atualizar e imprimir novos cartões de visita e/ou tirinhas com
      fingerprints.
    * Caso suas chaves sejam disponibilizadas via [WKD][] (Web Key Directory):
        * Proceda com a atualização correspondente, a depender de como o [WKD][]
          foi configurado para o domínio que você utiliza.
* Atualização de configurações:
    * Atualizar configuração de `~/.gnupg/gpg.conf`.
    * Desabilitar chave antiga usando `gpg --edit-key <fingerprint> disable`.
    * Caso sua chave seja usada para [assinatura em repositórios][git-sign] usando [Git][]:
        * Atualizar configurações do git global e por repositório (`user.signingkey`).
    * Se você mantém um registro de mudanças (por exemplo um arquivo
      `~/.gnupg/ChangeLog.md`), este é um bom momento para atualizá-lo.
* Backups:
    * Faça backups suficientemente seguros das suas chaves e atestado de
      transição.
* Caso você participe de listas [Schleuder][]:
    * Adicionar cópia da nova chave nas listas schleuder usando `x-add-key`.
      Pode ser necessária também a intervenção de alguém com acesso administrativo
      nas listas para fazer a transição.
* Caso o [keyringer][] seja utilizado:
    * Atualizar configurações de instâncias do [keyringer][].
* Caso o [mutt][] seja utilizado:
    * ajustar o parâmetro `pgp_sign_as` para a nova chave.
* Caso o [Monkeysphere][] seja utilizado:
    * No caso de transições de UIDs, rodar `sudo monkeysphere-authentication
      update-users` nos servidores que usem monkeysphere para autenticação.
* Revogação:
    * Após um tempo, revogue as chaves antigas.
    * Publique a chave revogada.
    * Faça um novo backup para registrar a revogação.

Referências:

* [OpenPGP DSA-1 key rollover](https://we.riseup.net/alster/openpgp-dsa1-key-rollover).

[mutt]: http://www.mutt.org/
[keyringer]: https://0xacab.org/keyringer
[Monkeysphere]: https://0xacab.org/monkeysphere/monkeysphere
[Git]: https://git-scm.com/
[git-sign]: https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work
[WKD]: https://wiki.gnupg.org/WKD

## Chaves OTR

### Renovação

Num shell:

    mv $HOME/.irssi/otr/otr.key $HOME/.irssi/otr/otr.key.old

No irssi, para cada uma das identidades que você utiliza:

    /otr genkey nick@network

A lista de identidades pode ser obtida no arquivo
`$HOME/.irssi/otr/otr.fp`.

Em seguida, informe pessoas sobre seu novo fingerprint OTR.

## Chaves de lista schleuder

1. Notificar listas sobre o rollover de chaves.
2. Crie uma nova chave para a lista, assinada pela anterior e com a
   mesma senha da anterior.
3. Importe o novo par de chaves no chaveiro do schleuder.
4. Atualize o fingerprint no `list.conf`.
5. Crie um `gpg.conf` para forçar o uso da nova chave,
   `default-key <key-fingerprint>`.
6. Notificar listas sobre o rollover de chaves.
7. Atualizar informações de contato sobre listas de interesse.

Após o procedimento, emails enviados tanto com a chave antiga
(compatibilidade) quanto com a nova funcionarão, porém é encorajado que
todo mundo passe a usar apenas a nova.

## Chaves SSH

Vide [Key rotation in OpenSSH 6.8+](http://blog.djm.net.au/2015/02/key-rotation-in-openssh-68.html).
