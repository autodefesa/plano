# Rede de Solidariedade Criptosindical

Este é um dos modelos possíveis de rede de solidariedade. Invente e
pratique os seus!

## Pressupostos

Não importa o que aconteça, pessoas vão rodar.

Se por um lado a martirização dessas pessoas pode contribuir para
comoção e reversão do quadro repressivo, sabemos historicamente que esse
efeito é muito secundário, é eticamente questionável e não compensa a
dor e a perda de importantes quadros. A reorganização da esquerda será
muito mais rápida caso tais pessoas tenham sua integridade física e
psicológica garantidas.

## Objetivos

Manter uma rede de solidariedade descentralizada, distribuída, que dê
conta de arrecadar e manter diversos recursos disponíveis e garantir que
cheguem a quem precisa com velocidade.

Ainda, a rede de solidariedade precisa ao mesmo tempo ser conhecida por
quem precisa mas extremamente discreta, para ela mesma não se tornar um
alvo da repressão.

Recursos são diversos e incluem:

* Dinheiro.
* Moradia temporária.
* Transporte e proteção.
* Assistência jurídica, psicológica e de saúde.

A rede deve ser eficiente: com o mínimo de trabalho necessário os
recursos devem ser arrecadados e atribuídos.

## Caráter Emergencial

Uma rede deste tipo pode existir permanentemente e sobreviver ao longo
de regimes políticos e humores sociais.

Ela também pode mudar para, em tempos mais normais, se transformar numa
geral para arrecadação e distribuição de recursos, onde pratica-se a
generosidade econômica de dividir a fartura e a bonança da superprodução
para quem precisa.

Ela também pode se transformar numa plataforma sociotécnica mais
sistematizada e automatizada.

Porém, no momento, a urgência e os requisitos de segurança e privacidade
requerem sua operação de forma artesanal.

## Protocolo de Operação

O ganho em eficiência e escala da Rede em relação à solidariedade
puramente espontânea se dá pela organização da forma como se segue.

Momentos:

1. Arrecadação de recursos: precisa ser feito constantemente.
2. Distribuição de recursos: feito de acordo com pedidos.

Papéis:

1. Núcleos de Arrecadação e Distribuição.
2. Rede de Apoio: pessoas e grupos que fornecem recursos e ajudam a
   fazer quem precisa entrar em contato.
3. Pessoas que são apoiadas.

Logística:

1. Recursos não precisam permanecer centralizados. A ideia é justamente
   que eles fiquem distribuídos de tal modo que possam chegar a
   qualquer local em tempo uniforme, isto é, sempre com a mesma
   agilidade.
2. A missão dos Núcleos é fazer a gestão dos recursos de forma mais
   segura possível, protegendo a identidade das pessoas e a localização
   de pessoas e recursos.

Requisitos de Participação:

1. Em Núcleos, apenas pessoas com suficiente estabilidade física e
   psicológica com perfil pragmático e tarefeiro.
2. Rede de Apoio: esquerda ampla, frentes anti-fascistas, indivíduos
   progressistas.
3. Pessoas apoiadas: quem estiver precisando e conseguir entrar em
   contato ou serem encontradas.

## Exemplo de Implementação

### 1. Núcleos

* Máximo de 7 participantes por Núcleo. O número 7 é importante pelas suas
  propriedades de reduzir polarizações (número primo, ímpar).
* Carga horária mínima de 4 horas semanais.
* 1 TPC e um celular mais seguro por participante.
* Para cada tarefa alocada são necessárias no mímimo duas pessoas para haver
  redundância.
* Rotatividade de tarefas.
* Comunicação interna usando criptografia razoável.
* Arquitetura da informação distribuída, sem dado disponível em servidor de
  forma descriptografada.
* Comunicação externa suportando tudo quanto é tipo de canal.
* Tarefas de rotina incluem a arrecadação e checagem de recursos disponíveis.

## TODO

* Tornar o texto fácil, acessível, curto e direto.
* Dar um exemplo mais concreto de funcionamento.
* Análise de ameaças:
  * Infiltração.
  * Corrupção/transparência.
  * Treta interna/degeneração.
  * Pedidos de ajuda falsos / DoS.
  * Vazamento de informação.
