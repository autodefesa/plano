# Política de Senhas

Senhas memorizadas:

1. Da chave OpenPGP principal.
2. Da BIOS e do bootloader dos [TPCs](tpc) ativos.
3. Dos volumes de partida dos [TPCs](tpc).
4. De login/pasta de usuário dos TPCs.
5. Tails persistent volume.

Senhas opcionais:

1. Da chave SSH principal.
2. De login/pasta de usuário de semi-TPCs.
3. Dos cofres, onde são armazenados TPCs, fingerprints trocados,
   documentos, etc.
4. Solitaire: criptografia analógica de fallback!

As demais senhas se encontram em gerenciadores de chave,
preferencialmente [keyringer](https://keyringer.pw).

## Justificativa e modelo de acesso

1. É necessário um invóluco de proteção mínimo para a chave OpenPGP
   principal, dada pelos volumes cifrados dos TPCs e da pasta de
   usuário.
2. A partir do acesso à chave OpenPGP, todas as outras senhas estarão
   acessíveis.
3. Usamos mais de um TPC para termos backups em caso de problemas. Cada
   TPC que não estiver sob a guarda do usuário deve estar armazenado
   num local seguro.
4. TPCs ativos necessitam de senhas de BIOS e
   [bootless](https://bootless.fluxo.info) para mitigar ataques do tipo
   Evil Maid e Warm Boot. Usar preferencialmente senhas distintas para
   cada TPC para reduzir danos caso sejam usados métodos de recuperação
   de senhas como o
   [cmospwd](https://packages.debian.org/stable/cmospwd).
5. Na ausência de TPCs, semi-TPCs podem ser utilizados desde que
   [bootless](https://bootless.fluxo.info). Por exemplo workstation de
   trabalho, shells remotas de usuário, etc.
6. Na indisponibilidade de TPCs ou semi-TPCs, o Tails deve ser
   utilizado. A senha do volume do Tails difere das dos volumes dos
   TPCs porque o Tails pode ser usado em não-TPCs.
7. Demais dispositivos apenas para realização de suporte técnico ou
   tarefas não-sensíveis, nos quais nenhuma das chaves acima
   mencionadas deverá ser utilizada.
8. Na ausência de equipamentos digitais, o Solitaire deve ser usado.

## Procedimento

### TODO e ChangeLog

* Manter um repositório git com tags assinadas com a chave OpenPGP
  principal.
* Opcionalmente, manter ambos arquivos diretamente no keyring pessoal.

### Criação

Para a criação de senha:

1. Pense na senha. Não a escreva! Conte apenas com a sua cabeça.
2. Nos três dias seguintes, tente recordar a senha. Caso não consiga,
   volte ao passo 1.
3. Caso você a tenha memorizado, proceda com a atualização da senha em
   questão.

### Usuário

Atualização diretamente no sistema, manifest do puppet, etc.

### Pastas pessoais

Isto precisa ser feito em sincronia com a mudança de senha de usuário no
sistema, caso contrário a montagem automática não vai funcionar:

    ecryptfs-rewrap-passphrase /home/.ecryptfs/$USER/.ecryptfs/wrapped-passphrase

### Volumes

Adicione a nova chave:

    cryptsetup luksAddKey <device>

Após um dado período de segurança, remova a chave anterior:

    cryptsetup luksRemoveKey <device>

Opcionalmente, fazer um backup do cabeçalho LUKS antes da mudança.

### Swap cifrada com senha conhecida (hibernate)

Como a swap não utiliza LUKS, é preciso criá-la novamente:

    swapoff -a
    cryptsetup remove cswap
    cryptsetup -h sha256 -c aes-cbc-essiv:sha256 -s 256 create cswap /dev/SUA_SWAP
    mkswap -f /dev/mapper/cswap

### Chave SSH

Mudando a senha de uma chave ssh privada:

    ssh-keygen -p -f ~/.ssh/<key>

### Chave OpenPGP

Mudando a senha de uma chave privada OpenPGP:

    gpg --edit-key <key> passwd

Ao mudar a senha de uma chave OpenPGP, lembre-se de mudar também as
senhas das chaves antigas:

    gpg --list-secret-keys
