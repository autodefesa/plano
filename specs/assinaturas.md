# Política de assinatura de chaves OpenPGP

## Texto padrão para envio de chaves locais

    Recentemente trocamos fingerprints de nossas chaves OpenPGP. Verifiquei
    o seu fingerprint e fiz uma assinatura local da sua chave, isto é, uma
    assinatura que fica apenas visível no meu chaveiro.

    Isso signifca que, por padrão, minha assinatura na sua chave não será
    exportada para os servidores de chave.

    Algumas pessoas preferem assinatura local para que o grafo social das
    assinaturas não fique público, então prefiro adotar essa abordagem por
    padrão caso a pessoa não mencione sua preferência.

    Para exportar a chave com a assinatura local, usei este comando:

      gpg --armor --export-options export-local-sigs --export <fingerprint-da-sua-chave>

    Para importar essa chave, basta salvá-la num arquivo e usar um comando
    do tipo

      gpg --import --import-options import-local < arquivo

    Você pode usar o mesmo comando de exportação para fornecer a minha
    assinatura para terceiros/as, mas caso você queira uma assinatura que
    seja exportável por padrão, basta entrar em contato.

## Texto padrão quando o fingerprint não pôde ser confirmado

    Recentemente trocamos fingerprints de nossas chaves OpenPGP, mas
    infelizmente não consegui confirmar o seu fingerprint.

    A chave que tenho possui o seguinte fingerprint:

      <fingerprint-chave>

    No entanto, no papel que você me deu o fingerprint é

      <fingerprint-papel>

    Ou seja, há uma diferença ``diferenca``. Enquanto a chave possui
    "``X``\ ", no papel tenho "``Y``\ ".

    Muito provavelmente isso é um erro de cópia do fingerprint, mas deixo a
    confirmação para uma próxima vez que nos encontrarmos ao vivo, pode ser?

## Texto padrão caso não haja cópia da chave

    Recentemente trocamos fingerprints de nossas chaves OpenPGP, mas
    infelizmente não consegui confirmar o seu fingerprint por falta de uma
    cópia da sua chave.

    Você poderia enviá-la? :)

## Nível de certificação

De `gpg(1)`:

    --ask-cert-level
    --no-ask-cert-level
           When making a key signature, prompt for a certification level. If this option is not specified, the certification level used is set via --default-cert-level. See --default-cert-level for information on the specific lev‐
           els and how they are used. --no-ask-cert-level disables this option. This option defaults to no.

    --default-cert-level n
           The default to use for the check level when signing a key.

           0 means you make no particular claim as to how carefully you verified the key.

           1 means you believe the key is owned by the person who claims to own it but you could not, or did not verify the key at all. This is useful for a "persona" verification, where you sign the key of a pseudonymous user.

           2 means you did casual verification of the key. For example, this could mean that you verified the key fingerprint and checked the user ID on the key against a photo ID.

           3  means you did extensive verification of the key. For example, this could mean that you verified the key fingerprint with the owner of the key in person, and that you checked, by means of a hard to forge document with
           a photo ID (such as a passport) that the name of the key owner matches the name in the user ID on the key, and finally that you verified (by exchange of email) that the email address on the key belongs to the key owner.

           Note that the examples given above for levels 2 and 3 are just that: examples. In the end, it is up to you to decide just what "casual" and "extensive" mean to you.

           This option defaults to 0 (no particular claim).

## URL de Política de Assinaturas

De `gpg(1)`:

    --set-policy-url string
           Use  string  as  a Policy URL for signatures (rfc4880:5.2.3.20).  If you prefix it with an exclamation mark (!), the policy URL packet will be flagged as critical. --sig-policy-url sets a policy url for data signatures.
           --cert-policy-url sets a policy url for key signatures (certifications). --set-policy-url sets both.
