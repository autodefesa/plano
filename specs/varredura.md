# Varredura Ambiental

Procedimento de varredura para detecção de "escutas ambientais".

## Considerações

Consideramos aqui que a escuta pode ser qualquer sensor, não apenas de
áudio ou vídeo, mas de qualquer outra variável ambiental, incluindo
sensores térmicos, de passagem, "sniffers" de rede wireless e
ethernet, bluetooth, NFC, etc.

Leve em conta que câmeras e alguns sensores necessitam de contato visual
com seus alvos, então parte deste tipo de grampo estará exposto, mesmo
que seja apenas um pequeno buraco numa parede, cano, móvel, etc.

Já as escutas sonoras e outros tipos de aparelhos podem ficar totalmente
encobertos. Eles apenas precisam que ondas sonoras e/ou eletromagnéticas
cheguem até eles.

## Suspeito que meu espaço tenha sido invadido. O que fazer?

Se seu espaço foi invadido e/ou suspeita que escutas tenham sido
instaladas:

1. Mantenha a calma. Qualquer estrago já foi feito. O que importa agora
   é reduzir os danos e evitar que a escuta persista.
2. Procure não usar o espaço para atividades sensíveis até realizar uma
   varredura.
3. Você tem a possibilidade de, ao encontrar uma escuta, tentar
   descobrir quem a instalou ou o que ela gravou. Tempo e discrição são
   fundamentais para isso.

## Preciso de especialistas para fazer varreduras?

Não necessariamente. Se você puder e quiser acionar especialistas, tudo
bem. Mas você também pode fazer por conta própria e com pouco treino.

Se for muito trabalho para você fazer, chame mais gente :)

## Existe método eficaz de detecção?

Não. Pode ser que você encontre, mas mesmo que não encontrar isso não
significa que não existam escutas. E mesmo que você encontre, nada
garante que você tenha encontrado todas as escutas existentes.

Este é [um problema essencial da
segurança](https://guia.autodefesa.org/limites.html).

Mesmo assim, vá em frente! :)

## Varredura sem equipamento especial

A primeira maneira de fazer varredura não conta com nenhum equipamento
de detecção que não a nossa inteligência e capacidade de mover, abrir e
fechar objetos.

A pergunta guia é: se fosse você a instalar escutas no local, onde as
colocaria?

* Conhecer os tipos de escutas mais comuns, visitando sites de venda destes
  produtos. Caso queira se aprofundar nestes conhecimentos, aprenda a construir
  sua própria escuta usando um mini-computador.
* Esvaziar todo o local.
* Inspecionar cada objeto retirado do local, procurando por escutas.  Todo
  objeto pode contar ou ser potencialmente uma escuta. A escuta pode ser
  inclusive algo considerado inútil ou desprezível como uma bolota de papel.
* Com o local vazio, passe a procurar por escutas nas instalações elétricas,
  hidráulicas, de ar condicionado, etc. Um local clássico de instalação de
  escutas é dentro das caixas de tomadas e de inspeção de conduítes
  (interruptores e tomadas de luz). Retire o "espelho" das tomadas e
  interruptores e verifique a existência de escutas instaladas. São locais
  muito comuns de instalação de escuta por já terem eletricidade disponível e
  um espaço razoável para acondicionar o equipamento.
* Lustres, buracos, frestas, cantos são ótimos locais. Pisos encaixados, forro
  de chão e de teto também são locais preferenciais para infestação de escutas.
  Hoje em dia existem câmeras embutidas até em lâmpadas!
* Abra os aparelhos telefônicos, computadores e demais eletrodomésticos. Muitos
  grampos físicos podem estar na sua frente e ter a aparência de dispositivos
  ou peças inofensivas, como por exemplo um conector para teclado.
* No caso de aparelhos eletrônicos que possuam câmeras e microfones, a escuta
  pode estar instalada via software.

Mesmo que você já tenha encontrado uma escuta, não interrompa a
varredura. Vá até o fim pois mais de uma escuta possa estar instalada.

Se o ambiente já é monitorado por alguma "escuta autorizada" como
câmera de vídeo em circuito fechado (CCTV), verifique se não há um
grampo/desvio/gato instalada na mesma e se o sistema de monitoramento
está protegido contra vírus de computador, vulnerabilidades de software,
falta de autenticação ou criptografia. Para um espião, pode ser muito
mais econômico explorar um sistema de vigilância já existente do que
instalar algo mais precário e com risco de ser facilmente identificável.

A meticulosidade ajuda muito na varredura. Sistemática também.

As escutas nem sempre estão instaladas no local espionado! Elas podem
ser sofisticadas como microfones a laser apontados para os vidros da
janelas para captarem as vibrações transmitidas do ar da sala para o
vidro.

## Varredura com equipamento especial

Neste caso, você usará alguma instrumentação. Pode ser algum equipamento
próprio para isso ou algo baseado na nossa inventividade.

Comecemos com a linha tosca, ops, básica de varredura! Estamos
interessados em equipamentos eletromecânicos e eletrônicos. Que em geral
possuem ao menos algumas partes metálicas e que utilizem eletricidade.

* Relógio de luz do local: se você desligar todos os seus equipamentos mas
  perceber que o relógio de luz ainda gira, mesmo que lentamente, então existe
  ainda algum equipamento não detectado funcionando.

  Esse método tem a falha de não detectar equipamentos que operam com baterias
  ou que retiram energia de outros sistemas, como por exemplo escutas
  telefônicas que aproveitam a própria tensão da linha de telefone para
  funcionar. Algumas escutas, mais raras, usam as próprias ondas
  eletromagnéticas como fonte de energia.

* Íman comum, que pode ser usado numa varredura simples de chão, paredes, forro
  do teto, objetos e móveis. O princípio é simples: havendo atração ou
  repulsão, algo metálico foi encontrado.

  A varredura pode ser prejudicada por um íman insuficientemente forte ou por
  barreiras como paredes muito espessas.

* Receptor de rádio ou telefone celular: podem ser usados para detectar
  emissões eletromagnéticas das escutas. Mas não são tão eficientes ou
  preparados quanto equipamentos profissionais e seu uso requer prática,
  habilidade e até arte.

* Uso de câmera térmica/infravermelho para detecção de equipamentos.

Já varredura profissional opera sob os mesmos princípios, porém usando
equipamentos próprios para essa atividade. Por exemplo os analisadores
de espectro eletromagnético possuem ótima sensibilidade ..

É possível construir seu próprio detector de escutas usando um hardware
SDR (Software Defined Radio) como aqueles bem baratos construídos a
partir do chip rtl2832, como é o caso do
[Salamandra](https://github.com/eldraco/Salamandra) (mais informações
[neste link](https://www.rtl-sdr.com/salamandra-a-modern-study-of-microhone-bugs-operation-and-detection-with-an-rtl-sdr/)
e [neste](https://www.rtl-sdr.com/salamandra-detecting-and-locating-spy-microphones-with-an-rtl-sdr/).

## A varredura precisa ser feita num veículo. Como fazer?

O princípio é análogo, mas dificuldado pela complicação de checar todas
as peças do automóvel e pela inviabilidade de desmontá-lo completamente.

Na dúvida, peça ajuda para alguém que entenda de mecânica e que possa ao
menos dar uma geral na cabine.

Um tipo de equipamento espião muito usado em veículos é o rastreador.

Automóveis recentes estão acompanhados por computadores de bordo. Na
verdade, hoje em dia automóveis são computadores com rodas. E esta é
outra fonte de vulnerabilidades.

## Como escolher manter um espaço livre de escutas?

Suponha que o local tenha muitos, muitos livros. E que você não tenha
condição de toda a vez fazer uma varredura pela bagunça que ela gera e
pelo trabalho que dá.

Algumas máfias usam a sauna pra fazer reuniões: todo mundo entra pelado
(menos lugares pra botar escutas), as paredes são sólidas e sem
reentrâncias.

Este exemplo pitoresco está longe de ser prático para a maioria dos
grupos.

A alternativa mais simples é não usar um espaço cheio de coisas para
fazer reuniões. Procure um espaço vazio, com menos objetos e menos
superfícies passíveis de instalação de escuta.

Alguns locais podem de antemão serem considerados públicos e portanto
impróprios para encontros sensíveis. Assume-se que neles é indiferente a
presença de escutas e que portanto não é necessário proceder com
varreduras.

Por isso, ao planejar atividades, leve em conta se ela precisa estar
livre de escutas e assim saiba se o lugar mais apropriado para
realizá-las seja algum com poucos objetos.

Se o local for usado seguidas vezes, crie um procedimento e especifique
uma periodicidade para realizar as varreduras.

Ao escolher o local, leve em conta também que as escutas podem ser
instaladas do lado de fora, como no caso dos microfones a laser. Locais
internos de um edifício podem ser menos vulneráveis a esse tipo de
ataque, por outro lado podem ser mais vulneráveis à propagação sonora
por dutos de ventilação, etc.

Alguns grupos tomam uma medida adicional quando se encontram: além de
usar um local menos vulneravel a escutas e de realizaram varreduras
periódicas, eles também evitam usar a fala para tratar de assuntos
sensíveis. Podem tanto usar computadores ligados em rede interna (LAN)
-- o que pode ter suas próprias vulnerabilidades -- ou até usam
medidas mais simples, de escrever em papéis -- tomando o cuidado de não
empilhá-los fazendo com que a escrita de uma página produza descalques
identificáveis nas páginas subsequentes -- e em seguida já queimá-los.
Cochicar no é do ouvido também é uma boa alternativa.

## O teatro musical da escuta

Bom, já falamos da parte mais metódica da coisa. Que tal nos divertirmos
um pouco também?

Se o seu negócio é fanfarronice com quem instalou a escuta, você pode
mandar uma mensagem especial usando o próprio aparelho dos espiões.
Nesse caso eles saberão que você sabe. Poderão até ficar furiosos.

Nem sempre recomendável, mas pode elevar sua moral e baixar a deles.

Você pode contar alguma anedota. Por exemplo a fabulosa históra [Da
Coisa](https://en.wikipedia.org/wiki/The_Thing_%28listening_device%29),
aparelho de escuta construído por Léon Theremin -- o mesmo que inventou
o instrumento musical theremin -- enquanto esteve preso num Gulag
durante a Guerra Fria. A escuta foi um "presente" dos russos para a
embaixada estado-unidense em Moscou e permaneceu indetectada por anos.

Alternativamente, você pode encontrar a escuta e fingir que não a
encontrou e combinar com seu grupo pra manter esse fingimento, criando
assim o Teatro da Escuta. Cuidado para que a ficção não contenha nenhum
traço das informações sensíveis que você deseja planejar. Também cuidado
para a performance não ser histriônica ou falsa demais, senão o boi na
linha pode sacar o que tá rolando.

Mas essa também pode ser a brincadeira: fornecer informações falsas até
que se liguem que estão sendo feitos de bobos.

## Referências

Veja na [fimografia](https://guia.autodefesa.org/referencias/filmografia.html)
a referência aos filmes "Sneakers", "The Conversation" e "A vida dos outros".
