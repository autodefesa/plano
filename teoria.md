# Teoria da Treta

## Conceito Geral

A dinâmica básica do modelo de ameaças é um conflito de interesses entre
atores, os Oponentes contra uma Entidade.

Os objetivos dos Oponentes são a neutralização da Entidade e vice-versa.
Para isso, cada ator utiliza artefatos tecnológicos (físicos, culturais,
etc). Os artefatos são classificados de acordo com a seguinte pergunta:

    Quem usa um artefato, como e contra quem?

Isso nos dá quatro elementos básicos:

    Quem usa    | Com qual finalidade | Então o artefato é considerado
    ------------------------------------------------------------------
    Oponente    |       ataque        |             ameaça
    Oponente    |       defesa        |             ameaça
    Entidade    |       ataque        |             ativo
    Entidade    |       defesa        |             mitigação

Por simplificação, consideramos uma ameaça qualquer artefato utilizado
pelos Oponentes, mesmo que estes sejam utilizados para defesa contra um
ataque da Entidade.

## Terminologia

* Entidade: ator principal do modelo, pode ser um indivíduo, uma organização,
  softwares ou processos. Em síntese, é um conjunto de mitigações.
* Ameaça: é qualquer forma de ataque contra a Entidade.
* Ativo: é tudo aquilo que pertence a Entidade e que ela identifica como de
  valor.
* Mitigação: é uma forma de dissipar uma ameaça.
* Oponente: ator que promove as ameaças e é definido por possuir interesses
  contrários ou conflitantes com outros atores. Em síntese, é um conjunto de
  ameaças.
* Risco: a possibilidade de uma ameaça ocorrer.

## Protocolo

### Elementos

Campos básicos presentes em todos os elementos:

* Dubin Core.
* Versão do protocolo utilizada.
* UUID de versão do elemento.
* URI: URI persistente, considerada como seu identificador único.
* URIs de requisitos (dependências).
* URIs de conflitos (com quais entidades esta não pode ser usada
  conjuntamente).

### Camada

Seguindo a estrutura do modelo OSI (Open System Interconnection), o
esquema é composto pelo empilhamento de camadas, na lógica de que um
ataque numa camada compromete a segurança de todas as camadas acima.
Campos adicionais:

* URI da camada inferior.

### Ativo

Propriedades:

* Tipo.
* Histórico.
* Quantidade.
* Importância.
* Ameaças (aponta para URIs de ameaças), isto é, faz ou está sujeito a algo que
  não desejamos?

### Ameaça

* URI de camada.
* Probabilidade ou nível da ameaça se concretizar: `P{0,1}`.
* Gravidade: o quanto a ameaça compromete os ativos.
* Mitigações (aponta para URIs de mitigações).

### Mitigação

* Custo.
* Eficácia: `P{0,1}`.
* Ameaças (aponta para URIs de ameaças).

### Entidade

É caracterizada por um conjunto de ativos, definidos tanto por ativos
que a entidade possui ou até pelos que ela não possui.

Campos adicionais:

* Ativos: lista de URIs de ativos.
* Inativos: lista de URIs de ativos que a entidade não possui.

### Oponente

É caracterizado por um conjunto de ameaças, definidas tanto por ameaças
que o oponente realizar ou até pelos quais ele não realiza.

Campos adicionais:

* Ameaças: lista de URIs de ameaças.
* Incapacidades: lista de URIs de ameaças que o oponente não realiza.

### Ferramenta

É caracterizada por um conjunto de mitigações, definidas tanto por
mitigações que a ferramenta implementa ou até pelas quais ela não
implementa.

Campos adicionais:

* Mitigações: lista de URIs de mitigações.
* Inefetividade: lista de URIs de mitigações não implementadas.

### Relação entre entidades

Entidades, Oponentes e Ferramentas são conjuntos dos elementos básicos,
então por simplificação podemos fazer um diagrama apenas com ativos,
ameaças e mitigações:

       .- mitigação 1
      /
     .- ameaça 1 -- mitigação 2
    /
    ativo --  ameaça 2 -- mitigação 3
    \
     `- ameaça n -- mitigação 4
      \
       `-- mitigação n

## Serialização

* YAML.
* JSON.
* XML.

## Queries

* RDF/SPARQL.
* XQuery/XPath?
