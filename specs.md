# Especificações

Especificações em Segurança Operacional são listas de procedimentos e
checklists padronizados para diversas atividades de defesa.

Este é um pequeno repositório de especificações de exemplo sobre
diversos procedimentos de segurança.

```{toctree}
:glob:
specs/*
```
